<?php
namespace Orpyca\webService;

/**
 * Instance available in all GraphQL resolvers as 3rd argument
 */
class AppContext
{
    /**
     * @var string
     */
    public $rootUrl;

    /**
     * @var User
     */
    public $viewer;

    /**
     * @var mixed
     */
    public $request;

    /**
     * @ORFEOConnect
     */
    public $oc;


    function __construct($objOrfeo) {
        $path = '..';
        require("$path/dbconfig.php");
        $this->oc = new ORFEOConnect($objOrfeo);
        $this->oc->setContentPath($CONTENT_PATH);
    }

}

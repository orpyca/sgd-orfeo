<?php

$ruta_raiz = "../";

session_start();
require_once($ruta_raiz."include/db/ConnectionHandler.php");

if (!$db){
    $db = new ConnectionHandler($ruta_raiz);
}

$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;

include ("common.php");
$fechah = date("ymd") . "_" . time("hms");

$params = session_name()."=".session_id()."&krd=$krd";
$url = "http://10.161.80.155:9999/SuperCor-WS/SuperCorWS?wsdl";

if(!empty($_POST['Busqueda']) && ($_POST['Busqueda']=="Busqueda"))
{
    $client = new SoapClient($url, array("trace" => 1, "exception" => 0));
    $result = null;
    $t = substr($nume_radi, 0, 1);
    $tipo = $t == '2' ? 'ConsultaNumeroRadSalida' : 'ConsultaNumeroRadicacion';

    $p = [
        'validador' => "BPM",
        'operador' => "9999",
        'rad_Numero' => $nume_radi,
        'dependencia' => ''
    ];

    $result = $client->__soapCall($tipo, ['parameters' => $p]);

    $data = $client->__soapCall('ConsultarImagenRadicado', ['parameters' => $p]);

    if (!file_exists($ruta_raiz.'/bodega/supercore')) {
        mkdir($ruta_raiz.'/bodega/supercore', 0777, true);
    }

    if(strlen($data->return)> 10) {
        $decoded = base64_decode($data->return);
        $file = $ruta_raiz.'/bodega/supercore/'.$nume_radi.'.pdf';
        file_put_contents($file, $decoded);
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once "$ruta_raiz/htmlheader.inc.php"; ?>
        <title>Consultas Expedientes</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="JavaScript" src="<?=$ruta_raiz?>/js/formchek.js"></script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="col-sm-12">
                <form action="supercor.php?<?=$params?>"
                    method="post" enctype="multipart/form-data"
                    class="form-horizontal"
                    name="formSeleccion" id="formSeleccion">
                    <section id="widget-grid" style="margin-top: 15px;">
                        <article>
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                                <header>
                                    <h2>
                                        B&uacute;squeda Supercor
                                    </h2>
                                </header>
                                <!-- widget content -->
                                <div class="widget-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Radicado </label>
                                            <input  class="form-control"
                                                    type="text"
                                                    name="nume_radi"
                                                    maxlength="17"
                                                    value="<?=$nume_radi?>"
                                                    autocomplete="off"
                                                    size="25">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input
                                                id="limpiar"
                                                class="btn btn-default"
                                                value="Limpiar"
                                                type="button">

                                            <input
                                                class="btn btn-primary"
                                                name="Busqueda"
                                                type="submit"
                                                id="envia22"
                                                value="Busqueda">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </form>
                <?php if($result) { ?>
                    <div class="row">
                        <section id="widget-grid">
                            <div class="col-md-12">
                                <article>
                                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false">
                                        <header>
                                            <h2>
                                                Resultados
                                            </h2>
                                        </header>
                                        <div class="widget-body">
                                            <div class="row">
                                                <?php if($result->return->codigoAccion !== ' ') { ?>
                                                    <div class="col-md-12">
                                                        Sin resultados (<?=$result->return->codigoAccion?>)
                                                    </div>
                                                <?php } else { ?>
                                                    <?php
                                                        $fechaRadicacion = DateTime::createFromFormat('Y-m-d\TH:i:sP', $result->return->fechaRadicacionRadicado);
                                                    ?>
                                                    <div class="col-md-12">
                                                        <h4>Radicado Nº <?=$result->return->numeroRadicado?> 
                                                            <small>
                                                                <?php if(strlen($data->return)> 10) { ?>
                                                                    <a href="supercord_fdl.php?<?=$params?>&num=<?=$nume_radi?>"><i class="icon-download-alt"></i>DESCARGAR</a>
                                                                <?php } ?>
                                                            </small>
                                                        </h4><br>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for=""><strong>Fecha radicación:</strong></label>
                                                        <p><small><?=$fechaRadicacion->format('Y-m-d H:i:s')?>&nbsp;</small></p>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for=""><strong>Dependencia remitente:</strong></label>
                                                        <p><small><?=$result->return->nombreDependenciaRemitente?>&nbsp;</small></p>
                                                    </div>
                                                    <?php if($t != '2') { ?>
                                                        <div class="col-md-12">
                                                            <label for=""><strong>Dependencia destino:</strong></label>
                                                            <p><small><?=$result->return->nombreDependenciaDestino?>&nbsp;</small></p>
                                                        </div>
                                                    <?php } else { ?>
                                                        <div class="col-md-12">
                                                            <label for=""><strong>Entidad destino:</strong></label>
                                                            <p><small><?=$result->return->nombreEntidadDestino?>&nbsp;</small></p>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="col-md-12">
                                                        <label for=""><strong>Estado:</strong></label>
                                                        <p><small><?=$result->return->estadoRadicado?>&nbsp;</small></p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for=""><strong>Remitente:</strong></label>
                                                        <p><small><?=$result->return->nombreRemitente?>&nbsp;</small></p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for=""><strong>Destino:</strong></label>
                                                        <p><small><?=$result->return->nombreDestino?>&nbsp;</small></p>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for=""><strong>Asunto:</strong></label>
                                                        <p><small><?=$result->return->asuntoRadicado?></small></p>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for=""><strong>Observaciones:</strong></label>
                                                        <p><small><?=$result->return->observacionesRadicado?>&nbsp;</small></p>
                                                    </div>
                                                    <?php if($t != '2') { ?>
                                                        <div class="col-md-6">
                                                            <label for=""><strong>Cedula remitente:</strong></label>
                                                            <p><small><?=$result->return->cedulaRemitente?>&nbsp;</small></p>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""><strong>Cedula destino:</strong></label>
                                                            <p><small><?=$result->return->cedulaDestino?>&nbsp;</small></p>
                                                        </div>
                                                    <?php } else { ?>
                                                        <div class="col-md-6">
                                                            <label for=""><strong>Dirección destino:</strong></label>
                                                            <p><small><?=$result->return->direccionDestino?>&nbsp;</small></p>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </section>
                    </div>
                <?php } ?>
            </div>
        </div>
        <script>
            $(function(){
                $('#limpiar').click(function(){
                    $(':input','#formSeleccion')
                        .not(':button, :submit, :reset, :hidden')
                        .val('')
                        .removeAttr('checked')
                        .removeAttr('selected');
                });
            });
        </script>
    </body>
</html>
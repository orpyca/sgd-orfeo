<?$ruta_raiz="../.."?> <html>
<head>
<title>Sticker web</title>
<link rel="stylesheet" href="estilo_imprimir.css" TYPE="text/css" MEDIA="print">
<style type="text/css">

body {
    margin-bottom:0;
    margin-left:0;
    margin-right:0;
    margin-top:0;
    padding-bottom:0;
    padding-left:0;
    padding-right:0;
    padding-top:0
    font-family: Arial, Helvetica, sans-serif;
}

.flex-container {
  padding: 0;
  margin: 0;
  list-style: none;
  -ms-box-orient: horizontal;
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -moz-flex;
  display: -webkit-flex;
  display: flex;
}

.wrap{
  -webkit-flex-wrap: wrap;
  flex-wrap: wrap;
}

.flex-item {
  text-align: center;
  margin-left: 0.5rem;
}
</style>
</head>

<body marginheight="0" marginwidth="0">
<ul class="flex-container wrap">
  <li class="flex-item">
    <?=$noRadBarras?>
  </li>
  <li class="flex-item">
    <img width="80" style="margin-top:7px;" src="<?=$dirLogo?>" alt="logo" />
  </li>
  <li class="flex-item">
    <font face="Arial" size="1">
        Por favor al contestar cite este número:
    </font>
  </li>
  <li class="flex-item">
	<font face="Arial" size="2"><?=$noRad?></font>
  </li>
  <li class="flex-item">
	<font face="Arial" size="1"><?=substr($radi_fech_radi,0,16)?></font>
  </li>
  <?php
    if(empty($dirLogo)){
        echo "
        <li class='flex-item'>
           <font face='Arial' size='1'><?=$entidad_corto?></font>
        </li> ";
    }

    if(!empty($anexos)){
        echo "
      <li class='flex-item'>
        <font face='Arial' size='1'>Anexos: <?=$anexos?>.</font>
      </li>";
    }

    if(!empty($folios)){
        echo "
      <li class='flex-item'>
	    <font face='Arial' size='1'>Folios: <?=$folios?>.</font>
      </li>";
    }
?>
  <li class="flex-item">
	<font face="Arial" size="1">Origen: <?=$remitente?>.</font>
  </li>
</ul>

</body>
</html>

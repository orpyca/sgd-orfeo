<?php
ini_set('display_errors',1);

$ruta_raiz = '..';
$saveFiles = "$ruta_raiz/bodega/cert/";

if ( !is_dir( $saveFiles ) ) {
    if (!mkdir($saveFiles, 0700)){
        die('Failed to create folders... /bodega/cert');
    }
}

$urlLogin    = 'https://microservice.vinkel.co/v1/auth/login';
$urlListCert = 'https://microservice.vinkel.co/v1/emailAPI/';
$urlCertDoc  = 'https://microservice.vinkel.co/v1/emailAPI/{{emailID}}/record/';
include_once("$ruta_raiz/include/db/ConnectionHandler.php");

$db = new ConnectionHandler("$ruta_raiz");

$dataCer     = array();

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $urlLogin);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

curl_setopt($ch, CURLOPT_POSTFIELDS,
    '{
    "email" : "superargo@supersalud.gov.co",
        "password" : "Super.2020*"
}');


//Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

//Execute the request.
$data = curl_exec($ch);

//Close the cURL handle.
curl_close($ch);

$arrLogin = json_decode($data);

if($arrLogin->statusCode !== 200){
    throw new Exception('No se realizo la consulta de login.');
}

$token = $arrLogin->result->token;

$ch = curl_init();
//Set the URL that you want to GET by using the CURLOPT_URL option.

$params   = http_build_query(array('startDate' => '2020-09-10',
                       'endDate' => '2020-09-16'));

curl_setopt($ch, CURLOPT_URL, $urlListCert.'?'.$params);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Token {$token}"));

curl_setopt($ch, CURLOPT_POSTFIELDS,);
//Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

//Execute the request.
$data = curl_exec($ch);

//Close the cURL handle.
curl_close($ch);

$arrCertificados = json_decode($data);

if($arrCertificados->statusCode !== 200){
    throw new Exception('No se realizo la consulta de login.');
}

foreach ($arrCertificados->result  as $value){
    $tmpUrl = $urlCertDoc;
    if(preg_match('/\d{12,}/', $value->subject, $output)){

        $newUrl = str_replace('{{emailID}}', $value->emailID, $urlCertDoc);

        $ch = curl_init();
        //Set the URL that you want to GET by using the CURLOPT_URL option.

        curl_setopt($ch, CURLOPT_URL, $newUrl );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Token {$token}"));

        curl_setopt($ch, CURLOPT_POSTFIELDS,);
        //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Execute the request.
        $data = curl_exec($ch);

        //Close the cURL handle.
        curl_close($ch);

        $data = json_decode($data);
        $namePath = $value->emailID. ".pdf";

        list($type, $data) = explode(';', $data->result->base64);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        if ($data === false) {
            throw new \Exception('base64_decode failed');
        }

        try{
            file_put_contents($saveFiles.$namePath, $data);
        } catch(Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $dataCer[] = array( 'cert_id_email'  => $value->emailID,
            'cert_name'      => $files->result->filename,
            'cert_path'      => $namePath,
            'cert_radi_nume' => $output[0]);


        $sel = "select
            count(1) as NUM
            from
            sgd_renv_regenvio
            where
            radi_nume_sal = {$output[0]}
            and sgd_renv_nombre = '{$value->emailID}'";

        $rs = $db->conn->query($sel);

        if ($rs && !$rs->EOF && $rs->fields["NUM"] > 0) {
            print_r("Registro Existente en _regenvio {$output[0]}");
            break;
        }

        $link  = "<a href=\"$saveFiles/$namePath\">Certificación del envio de correo</a>";
        $dateF = $db->conn->OffsetDate(0,$db->conn->sysTimeStamp);
        $email = $files->result->to;

        $db->conn->debug = true;

        $isql = "INSERT INTO SGD_RENV_REGENVIO(
            id,
            sgd_renv_pais,
            sgd_renv_cantidad,
            sgd_renv_depto,
            sgd_renv_mpio,
            sgd_renv_dir,
            sgd_dir_tipo,
            sgd_renv_mail,
            sgd_renv_codigo,
            sgd_renv_fech,
            radi_nume_sal,
            sgd_fenv_codigo,
            sgd_renv_nombre
        )VALUES(
            (select max(id) + 1 from sgd_renv_regenvio),
            'COLOMBIA',
            1,
            'D.C.',
            'BOGOTÁ',
            '$link',
             1,
            '$email',
            (select max(sgd_renv_codigo) + 1 from sgd_renv_regenvio),
            $dateF,
            '$output[0]',
            106,
            '$value->emailID'
        )";

        $rsInsert = $db->query($isql);
    }
}
